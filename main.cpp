#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

// distance of two aligned cameras
const double distance_cams_mm = 400.0;
const double working_distance_mm = 1880.0;
const double focal_length_mm = 100.0;
const double sensor_size_mm = 28.67;

cv::Mat stitch(cv::Mat& src1, cv::Mat& src2, int overlap_pixel) {
    cv::Mat panel;
    cv::copyMakeBorder(src1, panel, 0, 0, 0, src2.cols - overlap_pixel / 2,
                       cv::BORDER_CONSTANT, 0);
    double start = src1.cols - overlap_pixel / 2.0;
    int process_width = overlap_pixel;
    double alpha = 1.0;
    for (int i = 0; i < panel.rows; i++) {
        for (int j = start; j < panel.cols; j++) {
            if (panel.at<uchar>(i, j) == 0) {
                alpha = 1.0;
            } else {
                alpha = (j - start) * 2.0 / process_width;
            }
            panel.at<uchar>(i, j) = src2.at<uchar>(i, j - start) * alpha +
                    panel.at<uchar>(i, j) * (1.0 - alpha);
        }
    }
    return panel;
}

// assuming the setup of two line scan cameras are coplanar and aligned,
// we can simply stitch two N x 1 or N x M images with an overlap region.
// Due to the perspective projection in horizontal direction,
// we need to calculate the FOV(object_size_mm) with pre-determined paramters:
// working_distance_mm, focal_length_mm and sensor_size_mm.

int main() {
    cv::Mat src1(100, 800, CV_8U, 10);
    cv::Mat src2(100, 800, CV_8U, 220);
    double object_size_mm =
            sensor_size_mm * (working_distance_mm / focal_length_mm - 1);
    double overlap_mm = object_size_mm - distance_cams_mm;
    int overlap_pixel = overlap_mm / object_size_mm * src1.cols;
    cv::Mat out = stitch(src1, src2, overlap_pixel);
    cv::imshow("draw", out);
    cv::waitKey(0);
    return 0;
}
